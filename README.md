ReplicatorG64bit
================

The needed files to convert ReplicatorG to run on a 64bit Java VM

I created this project so that it is easy to upgrade jetty840/ReplicatorG to run on a 64bit Java VM.  I kept the directory structure the same as ReplicatorG.  The directory name replicatorg-0040r12-Sailfish will ofcourse be out of date when jetty840/ReplicatorG is upgraded

Please follow these steps to upgrade:
Check out this git project.
Go to the ReplicatorG directory on your PC, e.g C:\ReplicatorGSailfish\replicatorg-0040r12-Sailfish.  Rename rxtxSerial.dll to "rxtxSerial.dll 32bit" and j3dcore-ogl.dll to "j3dcore-ogl.dll 32bit".
Copy ReplicatorG.bat, rxtxSerial.dll and j3dcore-ogl.dll from the git project to C:\ReplicatorGSailfish\replicatorg-0040r12-Sailfish or where ever your ReplicatorG is located.
Change directory to lib, e.g C:\ReplicatorGSailfish\replicatorg-0040r12-Sailfish\lib
Rename j3dcore.jar, j3dutils.jar, RXTXcomm.jar and vecmath.jar.  Add 32bit after the extension on each file, e.g j3dcore.jar 32bit, RXTXcomm.jar 32bit
Copy j3dcore.jar, j3dutils.jar, RXTXcomm.jar and vecmath.jar to the lib from ReplicatorG64bit.

Change directory back to replicatorg-0040r12-Sailfish.
Make sure a 64bit Java is on the path.  If you downloaded and installed a 64bit version of Java it might be fine.  Test it by opening a command prompt (start|run|cmd) and type in java -version.  The message that is displayed should look something like this "Java HotSpot(TM) 64-Bit Server VM"
Run ReplicatorG.bat.
The bat file has been configured to start off using 2GB of memory up to a maximum of 4GB.